import scrapy
from scrapy import Request
from scrapy.spiders import CrawlSpider
from scrapy.exceptions import CloseSpider
from ..items import GoogleScraperItem
from .utils import check_crawled_url

ggs_domain = "https://scholar.google.com"

class GoogleSearchSpider(CrawlSpider):
    name = "google_search_crawler"
    start_urls = []

    def __init__(self, *a, **kw):
        super(GoogleSearchSpider, self).__init__(*a, **kw)
        domain_file = open("./data/domain_name.txt","r")
        dn = domain_file.read().split(',')
        for x in dn:
            self.start_urls.append('https://scholar.google.com/citations?view_op=search_authors&hl=vi&mauthors=label:{}'.format(x))
    
    def start_requests(self):
        for url in self.start_urls:
            yield self.make_requests_from_url(url)

    def make_requests_from_url(self, url):
        return Request(url, self.get_userlink)

    def get_userlink(self, response):
        selectors = response.xpath('//div[@class="gsc_1usr"]')
        for selector in selectors:
            link_params = selector.xpath('.//*[@class="gs_ai_name"]/a/@href').extract_first()
            link = ggs_domain + link_params
            yield Request(link,self.get_papers)
    def get_papers(self,response):
        selectors = response.xpath('//td[@class="gsc_a_t"]')
        for selector in selectors:
            describle_paper_param = selector.xpath('./a/@data-href').extract_first()
            describle_paper = ggs_domain + describle_paper_param
            yield Request(describle_paper,self.get_info_paper) 
    def get_info_paper(self,response):
        title = response.xpath('//*[@id="gsc_vcd_title"]/a/text()').extract_first()
        if title is None:
            title = response.xpath('//*[@id="gsc_vcd_title"]/text()').extract_first()
        if title is not None:
            print("title: ",title)
        
        describle = response.xpath('//*[@class="gsh_csp"]/text()').extract_first()
        if describle is None:
            describle = response.xpath('//*[@class="gsh_csp"]/text()').extract_first()
        # print(describle)
        print("---------------------------------------------")
